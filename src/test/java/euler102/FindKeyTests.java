package org.bitbucket.euler102;

import org.junit.Test;
import static org.junit.Assert.assertEquals;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.bitbucket.euler102.Cypher;

/**
 * Simple unit tests for finding the key
 */
public class FindKeyTests {

  /**
   *  Logger for this test suite
   */
  Logger logger = LoggerFactory.getLogger(FindKeyTests.class);

  private Cypher c = new Cypher();

  /**
   * Key of length 1
   */
  @Test
  public void findKeyOfLengthOne()  {
    assertEquals("*", c.findKey(1,"A","k"));
  }

  /**
   * Key of length 2
   */
  @Test
  public void findKeyOfLengthTwo()  {
    assertEquals("*", c.findKey(1,"A","k"));
  }
}
